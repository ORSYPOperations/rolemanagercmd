package com.orsyp.tools.rolemanager.actions;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.orsyp.Area;
import com.orsyp.api.Context;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.central.jpa.jpo.security.PermissionEntity;
import com.orsyp.central.jpa.jpo.security.RoleEntity;
import com.orsyp.central.jpa.jpo.security.RoleType;
import com.orsyp.comm.client.ClientServiceLocator;
import com.orsyp.tools.rolemanager.dispatcher.CommandParameters;
import com.orsyp.tools.rolemanager.login.Credentials;
import com.orsyp.tools.rolemanager.login.Login;

public class UvUpdatePermissionRole implements Action {
	CommandParameters allParams = new CommandParameters();

	public void run(Credentials cred, List<String> l) throws Exception {

		if (l.size() == 1) {
			displayHelp();
			return;
		}

		allParams.addParameter("ROLE", true);
		allParams.addParameter("SEARCH", true);
		allParams.addParameter("REPLACEWITH", true);

		// Generic arg processing Logic 

		for (int i = 1; i < l.size(); i++) 
			this.allParams.checkAndProcessArgument((String) l.get(i));		

		if (allParams.isThereMissingParams() == true) {
			System.err.println("ERROR - One or more mandatory parameter is missing:");
			this.allParams.getMissingParametersAsNames();
		}


		String RoleName = allParams.getParameterValueFromName("ROLE");
		String OldPermission = allParams.getParameterValueFromName("SEARCH");
		String NewPermission = allParams.getParameterValueFromName("REPLACEWITH");

		// --> Connection to UVMS

		Login myLog = new Login();
		myLog.getCentral(cred.getHostname(),cred.getLogin(), cred.getPassword(), cred.getPort());
		Context myCtx = myLog.getContext(cred.getHostname(), cred.getCompany(),Area.Exploitation, PRODUCT_TYPE.CENTRAL);


		// --> Actual Logic and Display of result starts here..
		ClientServiceLocator.setContext(myCtx);

		Collection<RoleEntity> allRoles = ClientServiceLocator.getSecurityRoleService().getAllRoles();

		boolean roleFound = false;
		Iterator<RoleEntity> it = allRoles.iterator();
		while (it.hasNext()) {

			RoleEntity myRole = it.next();

			myRole = ClientServiceLocator.getSecurityRoleService().getRoleByNameAndType(myRole.getName(),myRole.getRoleType());

			if (myRole.getName().equalsIgnoreCase(RoleName)) {
				roleFound = true;
				Set<PermissionEntity> myPermissions = ClientServiceLocator.getSecurityRoleService().getRoleNoGroupById(myRole.getId()).getPermissions();
				Set<PermissionEntity> myModifiedPermissions = new HashSet<PermissionEntity>();

				Iterator<PermissionEntity> itPerm = myPermissions.iterator();
				while (itPerm.hasNext()) {
					PermissionEntity myPerm = itPerm.next();
					if (myRole.getRoleType() == RoleType.OWLS) {
						if (myPerm.getProtectedResource().toString().contains(OldPermission)) { 
							myPerm.setProtectedResource(myPerm.getProtectedResource().toString().replace(OldPermission, NewPermission));
							System.out.println("Permission '" + myPerm.getPermissionClass()
									+ "' in role '" + myRole.getName()
									+ "' successfully updated. Replaced '" + OldPermission + "' with '" + NewPermission+"'");
						}
						myModifiedPermissions.add(myPerm);

					} else {
						System.out.println("Error: Only DUAS permissions can be changed.");
						System.exit(1);
					}
				}
				myRole.setPermissions(myModifiedPermissions);
				ClientServiceLocator.getSecurityRoleService().update(myRole);

				System.exit(0);
			}
		}
		if (!roleFound) {
			System.out.println("ERROR -  Role not found: " + RoleName);
		}

	}

	private void displayHelp() {
		System.out.println("Modify a specific permission within a role: replace a string pattern.");
		System.out.println("Parameters:");
		System.out.println("ROLE=<role name>");
		System.out.println("SEARCH=<original string>");
		System.out.println("REPLACEWITH=<replacement>");
		System.out.println("ex: ROLE PERMISSIONS ROLE=\"MyRole\" SEARCH=\"USER=*\" REPLACEWITH=\"USER=Admin\" ");
		System.exit(99);
	}
}
