package com.orsyp.tools.rolemanager.actions;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.orsyp.Area;
import com.orsyp.api.Context;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.api.central.services.ISecurityRoleService;
import com.orsyp.central.jpa.jpo.security.RoleEntity;
import com.orsyp.comm.client.ClientServiceLocator;
import com.orsyp.tools.rolemanager.dispatcher.CommandParameters;
import com.orsyp.tools.rolemanager.login.Credentials;
import com.orsyp.tools.rolemanager.login.Login;

public class UvUpdateRole implements Action {
	
	CommandParameters allParams = new CommandParameters();

	public void run(Credentials cred, List<String> l) throws Exception {
		
		if (l.size()==1) {
			displayHelp();
			return;
		}
		
		allParams.addParameter("ROLE", true);
		allParams.addParameter("LABEL", true);


		// Generic arg processing Logic Below
		for (int i = 1; i < l.size(); i++) 
			this.allParams.checkAndProcessArgument((String) l.get(i));

		if (allParams.isThereMissingParams() == true) {
			System.err.println("ERROR - One or more Mandatory Parameter is missing:");
			this.allParams.getMissingParametersAsNames();
		}

		// +++ RETRIEVE HERE THE VARIABLES YOU NEED FROM THE PARAMETERS
		String RoleName = allParams.getParameterValueFromName("ROLE");
		String NewLabel = allParams.getParameterValueFromName("LABEL");

		// --> Connection to UVMS
		Login myLog = new Login();
		myLog.getCentral(cred.getHostname(), cred.getLogin(), cred.getPassword(), cred.getPort());
		Context myCtx = myLog.getContext(cred.getHostname(), cred.getCompany(), Area.Exploitation, PRODUCT_TYPE.CENTRAL);

		// --> Actual Logic and Display of result starts here..
		ClientServiceLocator.setContext(myCtx);		
		ISecurityRoleService svc = ClientServiceLocator.getSecurityRoleService();

		Collection<RoleEntity> allRoles = svc.getAllRoles();

		Iterator<RoleEntity> it = allRoles.iterator();
		boolean foundRole = false;
		while (it.hasNext()) {
			RoleEntity myRole = it.next();
			myRole = svc.getRoleByNameAndType(myRole.getName(),myRole.getRoleType());
			if (myRole.getName().equalsIgnoreCase(RoleName)) {
				foundRole = true;
				myRole.setLabel(NewLabel);
				svc.update(myRole);
			}
		}
		if (!foundRole) {
			System.err.println("Error, could not find Role: " + RoleName);
			System.exit(1);
		}

		System.out.println("Role: " + RoleName + " Updated Successfully!");
		System.exit(0);
	}

	private void displayHelp() {
		System.out.println("Update an Existing Role.");
		System.out.println("Parameters:");
		System.out.println("ROLE=<role name>");
		System.out.println("LABEL=<new label>");
		System.out.println("ex: UPDATE ROLE=\"MyRole\" LABEL=\"My New Label\" ");
		System.exit(99);
	}
}
