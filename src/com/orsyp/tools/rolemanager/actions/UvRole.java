package com.orsyp.tools.rolemanager.actions;

import java.util.Arrays;
import com.orsyp.tools.rolemanager.login.Credentials;

public class UvRole {
	
	public static void objectDispatchAction(Credentials uvCred, String[] args) throws Exception {

		if (args.length == 0) {
			System.err.println("ERROR - Wrong Syntax, no ACTION passed");
			System.err.println("List of valid actions:\n");
			getListObjects();
			System.exit(1);
		}

		String OPERATION_TYPE = args[0].toString();		
		try {
			RoleActions.valueOf(OPERATION_TYPE.toUpperCase());
		} catch (IllegalArgumentException i) {
			System.err.println("ERROR - Wrong action: " + OPERATION_TYPE + " is not recognized");
			System.err.println("List of available actions :");
			getListObjects();
			System.exit(2);
		}

		try {
			Action action = (Action) RoleActions.valueOf(OPERATION_TYPE.toUpperCase()).getAClass().newInstance();					
			action.run(uvCred, Arrays.asList(args));			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void getListObjects() {
		for (int i = 0; i < RoleActions.values().length; i++) 
			System.err.printf("  %-20s   %-100s%n", RoleActions.values()[i].name(), RoleActions.values()[i].getLabel());
	}
}
