package com.orsyp.tools.rolemanager.actions;

public enum RoleActions {
	UPDATE(1, "Update a Role", UvUpdateRole.class),  //"com.orsyp.rolemanager.actions.UvUpdateRole" 	
	DUPLICATE(5,"Duplicate a Role", UvDuplicateRole.class ), //"com.orsyp.rolemanager.actions.UvDuplicateRole" 
	PERMISSIONS(6,"Update Specific Permission in Role", UvUpdatePermissionRole.class ); //"com.orsyp.rolemanager.actions.UvUpdatePermissionRole" 
	
	private int code;
	private String label;
	private Class<?> aClass;

	private RoleActions(int code, String label, Class<?> aClass) {
        this.code = code;
        this.label = label;
        this.aClass=aClass;

    }
	

 	public int getCode() {
        return code;
    }
 
    public String getLabel() {
        return label;
    }
    public Class<?> getAClass() {
        return this.aClass;
    }
}
