package com.orsyp.tools.rolemanager.actions;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.orsyp.Area;
import com.orsyp.api.Context;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.central.jpa.jpo.GroupRoleProxyEntity;
import com.orsyp.central.jpa.jpo.security.PermissionEntity;
import com.orsyp.central.jpa.jpo.security.RoleEntity;
import com.orsyp.comm.client.ClientServiceLocator;
import com.orsyp.tools.rolemanager.dispatcher.CommandParameters;
import com.orsyp.tools.rolemanager.login.Credentials;
import com.orsyp.tools.rolemanager.login.Login;

public class UvDuplicateRole implements Action {

	CommandParameters allParams = new CommandParameters();

	public void run(Credentials cred, List<String> l) throws Exception {		
		
		if (l.size()==1) {
			displayHelp();
			return;
		}		

		allParams.addParameter("ROLE", true);
		allParams.addParameter("NEWROLE", true);


		// Generic arg processing Logic Below
		for (int i = 1; i < l.size(); i++) 
			this.allParams.checkAndProcessArgument((String) l.get(i));
		

		if (allParams.isThereMissingParams() == true) {
			System.err.println("ERROR - One or more Mandatory Parameter is missing:");
			this.allParams.getMissingParametersAsNames();
		}

		String SourceRoleName = allParams.getParameterValueFromName("ROLE");
		String NewRoleName = allParams.getParameterValueFromName("NEWROLE");


		Login myLog = new Login();
		myLog.getCentral(cred.getHostname(), cred.getLogin(), cred.getPassword(), cred.getPort());
		Context myCtx = myLog.getContext(cred.getHostname(), cred.getCompany(), Area.Exploitation, PRODUCT_TYPE.CENTRAL);

		// --> Actual Logic and Display of result starts here..
		ClientServiceLocator.setContext(myCtx);
		Collection<RoleEntity> allRoles = ClientServiceLocator.getSecurityRoleService().getAllRoles();
		Iterator<RoleEntity> it = allRoles.iterator();
		boolean isFound = false;

		while (it.hasNext()) {
			RoleEntity ModelRole = it.next();
			if (ModelRole.getName().equalsIgnoreCase(SourceRoleName)) {
				isFound = true;
				RoleEntity TargetRole = new RoleEntity();

				// Getting Permissions from old role, initializing permissions
				// from new role
				Set<PermissionEntity> ModelSetPermissions = ClientServiceLocator.getSecurityRoleService().getRoleNoGroupById(ModelRole.getId()).getPermissions();
				Set<PermissionEntity> TargetSetPermissions = new HashSet<PermissionEntity>();

				Iterator<PermissionEntity> ModelPermissionIterator = ModelSetPermissions.iterator();
				// For each existing Permissions..
				while (ModelPermissionIterator.hasNext()) {

					PermissionEntity OnePermissionEnt = (PermissionEntity) ModelPermissionIterator.next();
					PermissionEntity TargetPermissionEnt = new PermissionEntity();

					// We duplicate each attribute into the new permission and
					// add to the target set of permissions
					TargetPermissionEnt.setActionName(OnePermissionEnt.getActionName());
					TargetPermissionEnt.setPermissionClass(OnePermissionEnt.getPermissionClass());
					TargetPermissionEnt.setPositive(OnePermissionEnt.isPositive());
					TargetPermissionEnt.setProtectedResource(OnePermissionEnt.getProtectedResource());
					TargetSetPermissions.add(TargetPermissionEnt);

				}

				String TargetRoleName = NewRoleName;
				RoleEntity tst = ClientServiceLocator.getSecurityRoleService().getRoleByNameAndType(TargetRoleName,ModelRole.getRoleType());
				if (tst != null) {
					System.out.println("Error: Role: " + TargetRoleName+ " already exists!");
					System.exit(5);

				} else {
					TargetRole.setName(TargetRoleName);
					TargetRole.setRoleType(ModelRole.getRoleType());
					TargetRole.setArea(ModelRole.getArea());
					TargetRole.setCompany(ModelRole.getCompany());
					TargetRole.setNodeType(ModelRole.getNodeType());
					TargetRole.setNodeName(ModelRole.getNodeName());
					TargetRole.setGroupRoleProxies(new HashSet<GroupRoleProxyEntity>());

					TargetRole.setPermissions(TargetSetPermissions);

					TargetRole = ClientServiceLocator.getSecurityRoleService().create(TargetRole);

					System.out.println("Role duplicated: "+ TargetRoleName);
				}

			}
		}
		if (!isFound) {
			System.err.println("ERROR - Role: " + SourceRoleName+ " not found.");
			System.exit(1);
		}

		System.exit(0);
	}

	private void displayHelp() {
		System.out.println("Duplicate existing role.");
		System.out.println("Parameters:");
		System.out.println("ROLE=<role name>");
		System.out.println("NEWROLE=<new role name>");
		System.out.println("example: DUPLICATEROLE ROLE=\"MyRole\" NEWROLE=\"MyNewRole\"");
		System.exit(99);
	}
}
