package com.orsyp.tools.rolemanager.actions;

import java.util.List;
import com.orsyp.tools.rolemanager.login.Credentials;

public interface Action {
	
	public void run(Credentials cred, List<String> l) throws Exception;

}
