package com.orsyp.tools.rolemanager.dispatcher;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class CommandParameters {

	/**
	 * Two Maps are defined, one containing all Mandatory Parameters for command
	 * line, the other one containing Optional Parameters
	 */

	Map<String, String> MandatoryParams = new HashMap<String, String>();
	Map<String, String> OptionalParams = new HashMap<String, String>();


	// add parameter definition to either Map
	public void addParameter(String name, boolean isMandatory) {

		if (isMandatory) {
			MandatoryParams.put(name, "");
		} else {
			OptionalParams.put(name, "");
		}

	}

	// get all parameters from either Map
	public Map<String, String> getParameters(boolean isMandatory) {

		if (isMandatory) {
			return MandatoryParams;
		} else {
			return OptionalParams;
		}

	}

	// get the list of parameters (keys) of either Map
	public Set<String> getParametersAsNames(boolean isMandatory) {

		if (isMandatory) {
			return MandatoryParams.keySet();
		} else {
			return OptionalParams.keySet();
		}

	}

	// check if a parameter passed is valid.. NOTE: arg has format NAME=VALUE
	public boolean checkAndProcessArgument(String arg) { // arg --> NAME=VALUE
		if ((arg.contains("=")) && (arg.split("=").length >= 2)) {

			String VarName = arg.split("=")[0];
			String VarValue = arg.replace(VarName+"=","");

			if (this.MandatoryParams.containsKey(VarName.toUpperCase()) || this.OptionalParams.containsKey(VarName.toUpperCase())) {

				if (this.MandatoryParams.containsKey(VarName.toUpperCase())) {
					this.MandatoryParams.remove(VarName.toUpperCase());
					this.MandatoryParams.put(VarName.toUpperCase(), VarValue);
				}

				if (this.OptionalParams.containsKey(VarName.toUpperCase())) {
					this.OptionalParams.remove(VarName.toUpperCase());
					this.OptionalParams.put(VarName.toUpperCase(), VarValue);
				}

				return true;
			} else {
				System.err.println("ERROR - invalid arguments\n");

				System.err.println("List Of Mandatory Parameters:");
				System.err.println(this.MandatoryParams.keySet().toString());

				System.exit(2);
				return false;
			}

		} else {
			System.err.println("ERROR - all Parameters must follow the format: PARAMETERNAME=VALUE (ex: LOGIN=lilly)");
			System.exit(2);
			return false;
		}

	}

	// Upon validation of a parameter
	public void setParameterValue(String arg) {
		if (arg.contains("=")) {
			String VarName = arg.split("=")[0];
			String VarValue = arg.split("=")[1];

			if (this.MandatoryParams.containsKey(VarName.toUpperCase())) {
				this.MandatoryParams.remove(VarName.toUpperCase());
				this.MandatoryParams.put(VarName.toUpperCase(), VarValue);
				// return VarValue;
			}
			if (this.OptionalParams.containsKey(VarName.toUpperCase())) {
				this.OptionalParams.remove(VarName.toUpperCase());
				this.OptionalParams.put(VarName.toUpperCase(), VarValue);
				// return VarValue;
			}

		}

		else {
			System.err.println("ERROR - all Parameters must be in the following format: PARAMETERNAME=VALUE");
			System.exit(2);
			// return "";
		}
		// return "";
	}

	public String getParameterValueFromName(String VarName) {
		if (this.MandatoryParams.containsKey(VarName.toUpperCase())) {
			return this.MandatoryParams.get(VarName.toUpperCase());
		}
		if (this.OptionalParams.containsKey(VarName.toUpperCase())) {
			return this.OptionalParams.get(VarName.toUpperCase());
		}
		return "";
	}

	public boolean isThereMissingParams() {
		if (this.MandatoryParams.containsValue("")) {
			return true;
		} else {
			return false;
		}
	}

	public void getMissingParametersAsNames() {

		Iterator<Entry<String, String>> it = this.MandatoryParams.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> pairs = it.next();
			if (pairs.getValue().equals("")) 
				System.err.println(" --> Missing Parameter: " + pairs.getKey() + "=?");
			// System.out.println(pairs.getKey() + " = " + pairs.getValue());
			it.remove();
		}
		Iterator<Entry<String, String>> it2 = this.OptionalParams.entrySet().iterator();
		while (it2.hasNext()) {
			Entry<String, String> pairs =  it2.next();
			if (pairs.getValue().equals("")) {
				System.err.println(" --> Optional Parameter: " + pairs.getKey()
						+ "=?");
			}
			// System.out.println(pairs.getKey() + " = " + pairs.getValue());
			it2.remove();
		}
		System.exit(4);
	}
}
