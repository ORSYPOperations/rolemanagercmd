package com.orsyp.tools.rolemanager.dispatcher;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import com.orsyp.SyntaxException;
import com.orsyp.tools.rolemanager.login.Credentials;

public class UvVersion {
	
	public final static String VersionTool = "0.1";
	public final static String APIVersion  = "4.0.15";
	public final static String APIVersionShort  = "4";
	
	public static void objectDispatchAction(Credentials uvCred, List<String> args, boolean help) throws IOException, InterruptedException, SyntaxException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException{

		if(args.size()==2 && args.get(1).equalsIgnoreCase("ORSYP")){ 
			System.out.println(",");
			System.exit(1);
		}
		
		System.out.printf(" %-30s   %-10s%n","UVMS Cmd Line Utility Version:", VersionTool);
		System.out.printf(" %-30s   %-10s%n","UniViewer API Version:", APIVersion);
		System.exit(0);
		
	}
}
