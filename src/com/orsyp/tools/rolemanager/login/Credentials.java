package com.orsyp.tools.rolemanager.login;

public class Credentials {

	private String login;
	private String password;
	private String hostname;
	private int port;
	private String company;

	/*
	 * The Company string does not really matter.. it is only used as a value to
	 * instanciate a context for some of the commands, but since we are working
	 * with UVMS, it does not need to actually exist
	 */

	public Credentials(String l, String p, String h, int po) {
		this.login = l;
		this.password = p;
		this.hostname = h;
		this.port = po;
		this.company = "UNIV99";

	}

	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

}
