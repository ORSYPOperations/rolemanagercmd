package com.orsyp.tools.rolemanager.login;

import java.io.IOException;
import com.orsyp.Area;
import com.orsyp.Environment;
import com.orsyp.Identity;
import com.orsyp.SyntaxException;
import com.orsyp.UniverseException;
import com.orsyp.api.Client;
import com.orsyp.api.Context;
import com.orsyp.api.central.UniCentral;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.std.ClientConnectionManager;
import com.orsyp.std.EnvironmentStdImpl;
import com.orsyp.std.MultiCentralConnectionFactory;
import com.orsyp.std.central.UniCentralStdImpl;

public class Login {

	int UJPort;
	private UniCentral uniCentral;

	public UniCentral getCentral(String UVMSHost, String login, String password, int port) throws IOException, InterruptedException {
		// UniCentral uniCentral=null;
		try {
			uniCentral = new UniCentral(UVMSHost, port);
			uniCentral.setImplementation(new UniCentralStdImpl(uniCentral));
			uniCentral.login(login, password);
			if (ClientConnectionManager.getDefaultFactory() == null)
				ClientConnectionManager.setDefaultFactory(MultiCentralConnectionFactory.getInstance());

			uniCentral.authenticate();
			// System.out.println ("Authentication successful for user " +
			// login);

		} catch (UniverseException e) {
			System.err.println("-- ERROR: Authentication Failed. Please check message below:\n\n");
			System.out.println(e.getMessage());
			System.exit(1);
		}
		return uniCentral;
	}

	public Context getContext(String nodeName, String company, Area area, PRODUCT_TYPE productType) throws SyntaxException {

		Environment environment = new Environment(company, nodeName, area);
		Identity identity = new Identity(this.uniCentral.getLogin(), "*", "*","WEB");
		Client client = new com.orsyp.api.Client(identity);
		Context context = new com.orsyp.api.Context(environment, client);
		context.setImpl(new EnvironmentStdImpl());
		context.setUnijobCentral(uniCentral);
		context.setProduct(productType.getProduct());
		return context;
	}
}
