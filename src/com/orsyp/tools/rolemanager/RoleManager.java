package com.orsyp.tools.rolemanager;

import java.util.Properties;

import com.orsyp.UniverseException;

import com.orsyp.api.central.UniCentral;
import com.orsyp.tools.rolemanager.actions.UvRole;
import com.orsyp.tools.rolemanager.dispatcher.UvVersion;
import com.orsyp.tools.rolemanager.login.Credentials;
import com.orsyp.tools.rolemanager.login.Login;
import com.orsyp.tools.rolemanager.utils.Utils;


public class RoleManager {
	public static void main(String[] args) throws Exception {

		// Was an object type passed as a parameter?
		if (args.length == 0) {
			System.err.println("Parameters: [ACTION] [PARAM1=something PARAM2=...]");
			System.err.println("Available actions: UPDATE, DUPLICATE, PERMISSIONS");
			System.err.println("Launch an action without parameters to show help");	
			System.exit(1);
		}

		Properties pGlobalVariables = Utils.loadFile("cmdConfig.properties", false);
		String UvmsLogin = pGlobalVariables.getProperty("UVMS_LOGIN");
		String UvmsPassword = pGlobalVariables.getProperty("UVMS_PWD");
		String UvmsPortAsStr = pGlobalVariables.getProperty("UVMS_PORT");
		String UvmsHost = pGlobalVariables.getProperty("UVMS_HOST");

		if (UvmsLogin == null || UvmsLogin.equals("")) {
			System.err.println("Error, No UVMS Login Specified.");
			System.exit(98);
		}
		if (UvmsPassword == null || UvmsPassword.equals("")) {
			System.err.println("Error, No UVMS Password Specified.");
			System.exit(97);
		}
		if (UvmsHost == null || UvmsHost.equals("")) {
			System.err.println("Error, No UVMS Hostname or IP Specified.");
			System.exit(96);
		}
		if (UvmsPortAsStr == null || UvmsPortAsStr.equals("")) {
			System.err.println("Error, No UVMS Communication Port Specified.");
			System.exit(95);
		}

		Credentials uvCred = new Credentials(UvmsLogin, UvmsPassword, UvmsHost,Integer.parseInt(UvmsPortAsStr));

		Login myLog = new Login();
		UniCentral myCentral = myLog.getCentral(uvCred.getHostname(),uvCred.getLogin(), uvCred.getPassword(), uvCred.getPort());
		String UvmsVersion = "";
		try {
			UvmsVersion = Integer.toString(myCentral.getVersion()).substring(0,1);
		} catch (UniverseException e1) {
			e1.printStackTrace();
		}

		if (!UvmsVersion.equals(UvVersion.APIVersionShort)) {
			System.err.println("!! Warning - API Version [v" + UvVersion.APIVersionShort + "] and UVMS Version [v" + UvmsVersion + "] do not match.. ");
			System.err.println("!! Warning - This Operation might fail.");
		}

		UvRole.objectDispatchAction(uvCred, args);

	}

}
