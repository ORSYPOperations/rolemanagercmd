package com.orsyp.tools.rolemanager.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Utils {

	public static Properties loadFile(String s, boolean debug) {
		String path = null;
		String FilePath = null;
		Properties prop = new Properties();
		try {
			path = new File(".").getCanonicalPath();
			FilePath = path + File.separator + s;
			if (debug) 
				System.out.println("DEBUG:" + FilePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		File myFile = new File(FilePath);
		InputStream in = null;
		try {
			in = new FileInputStream(myFile);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		try {
			prop.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}
}
